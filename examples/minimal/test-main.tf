terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-gitlab-project-minimal"
    }
  }
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

module "test" {
  source = "../.."

  parent_group_ids = [5752175]
  unique_groups    = 1

  cicd_project_path = ""

  projects = [
    {
      #      parent_group_id            = 5752175
      name                       = "project-tf-module-example-minimal",
      path                       = "",
      description                = "",
      project_kind               = "",
      visibility_level           = "",
      merge_method               = "",
      container_registry_enabled = false,
      issues_enabled             = false,
      wiki_enabled               = false,
      snippets_enabled           = false,
    }
  ]

}
