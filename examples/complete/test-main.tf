terraform {
  backend "remote" {
    organization = "open-source-devex"
    workspaces {
      name = "terraform-modules-gitlab-project-complete"
    }
  }
}

variable "gitlab_token" {
  type = string
}

provider "gitlab" {
  token = var.gitlab_token
}

module "test" {
  source = "../.."

  parent_group_ids = [5785002, 5785002, 5785004]
  unique_groups    = 2

  cicd_project_path = "0-cicd"

  create_cicd_deploy_key = true
  cicd_deploy_key        = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDE5COWbGExCQGQq4v4LI4jE/iJyrwNS8qOT8NcAwoOGDlpTG6qpMhDdyduOofSUT/2r7eyfBK3UsvgnPxqcgeqYvYpVlQQPbmYZ50kgpZam/sulfnwptrHhPfm/KBdN9LzrYmpwE4gKUrvfOU7+MeYalVEQthi2viJq1SZCv7tmw=="
  cicd_deploy_key_name   = "Key used in testing of TF modules"

  projects = [
    {
      name                       = "project-tf-module-example-complete-1",
      path                       = "project-tf-module-example-complete-1-path",
      description                = "Complete example of Gitlab project1",
      project_kind               = "terraform-module",
      visibility_level           = "private",
      merge_method               = "merge",
      container_registry_enabled = true,
      issues_enabled             = true,
      wiki_enabled               = true,
      snippets_enabled           = true,
    },
    {
      name                       = "project-tf-module-example-complete-3",
      path                       = "project-tf-module-example-complete-3-path",
      description                = "Complete example of Gitlab project3",
      project_kind               = "terraform-module",
      visibility_level           = "private",
      merge_method               = "merge",
      container_registry_enabled = true,
      issues_enabled             = true,
      wiki_enabled               = true,
      snippets_enabled           = true,
    },
    {
      name                       = "project-tf-module-example-complete-2",
      path                       = "project-tf-module-example-complete-2-path",
      description                = "Compelte example of Gitlab project2",
      project_kind               = "docker",
      visibility_level           = "public",
      merge_method               = "merge",
      container_registry_enabled = true,
      issues_enabled             = true,
      wiki_enabled               = true,
      snippets_enabled           = true
    },
  ]
}

output "repository_clone_urls_per_group" {
  value = module.test.repository_clone_urls_per_group
}
