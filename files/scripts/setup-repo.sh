#!/usr/bin/env bash

set -e

add-submodule() {
  submodule_local_path="${1}"
  submodule_remote_path="${2}"

  if [[ -n "${submodule_local_path}" && -z "$( git submodule status |  grep ${submodule_local_path} )" ]]; then
    git submodule add ${submodule_remote_path} ${submodule_local_path}
    git commit -m "Add submodule ${submodule_local_path}"
   else
    echo "==> Submodule ${submodule_local_path} already exists"
  fi
}

commit-file() {
  commit_message="${1}"
  name="${2}"
  content="${3}"

  # file is not a link nor regular file
  if [[ ! (-h ${name} || -f ${name}) ]]; then
    touch ${name}
    echo "${content}" > ${name}
    git add ${name}
    git commit -m "${commit_message}"
  else
    echo "==> File ${name} already exists"
  fi
}

make-file-executable() {
  name="${1}"

  if [[ ! -x ${name} ]]; then
    chmod +x ${1}
    git commit -a --amend --no-edit
  else
    echo "==> File ${name} already executable"
  fi
}

commit-terraform-common-files() {
  commit-file "Add Makefile" Makefile "$( cat ${SCRIPT_DIR}/../project-templates/Makefile )"
  commit-file "Add versions.tf" "versions.tf" "$( cat ${SCRIPT_DIR}/../project-templates/versions.tf )"
  commit-file "Add main.tf" "main.tf"
  commit-file "Add variables.tf" "variables.tf"
  commit-file "Add outputs.tf" "outputs.tf"
}

SCRIPT_DIR="$( cd "$( dirname "${0}" )" && pwd )"

PROJECT_KIND="${1}"
CICD_PROJECT_LOCAL_PATH="${2}"
CICD_PROJECT_REMOTE_PATH="${3}"

if [[ "${PROJECT_KIND}" != "" ]]; then
  commit-file "Add version file" "version.txt" "1.0.0"
  [[ -f ${SCRIPT_DIR}/../gitlab-ci/${PROJECT_KIND}.yml ]] && commit-file "Add gitlab-ci.yml" ".gitlab-ci.yml" "$( cat ${SCRIPT_DIR}/../gitlab-ci/${PROJECT_KIND}.yml )"
  [[ -f ${SCRIPT_DIR}/../gitignore/${PROJECT_KIND} ]] && commit-file "Add gitignore file" ".gitignore" $( cat ${SCRIPT_DIR}/../gitignore/${PROJECT_KIND} )

  case ${PROJECT_KIND} in
    docker)
      mkdir -p container
      commit-file "Add Dockerfile" "container/Dockerfile" "FROM alpine:latest"
      commit-file "Add run-tests-sh script" "run-tests.sh"
      make-file-executable "run-tests.sh"
      ;;
    terraform-module)
      commit-terraform-common-files
      ;;
    terraform)
      commit-terraform-common-files
      ;;
    packer)
      echo "No packer support yet"
      ;;
    *)
      echo "Project kind not recognized: ${PROJECT_KIND}"
  esac
fi

commit-file "Add README.md" "README.md"

if [[ "${PROJECT_KIND}" != "cicd" ]]; then
  add-submodule "${CICD_PROJECT_LOCAL_PATH}" "${CICD_PROJECT_REMOTE_PATH}"
fi

# push if remote branch does not exist or if there are new commits
([[ -z "$( git ls-remote --heads --quiet | grep "refs/heads/master" )" ]] || [[ -n "$( git --no-pager log origin/master..master 2> /dev/null || true )" ]]) && git push || true
