# project

Terraform module to create gitlab projects.
Since it's not possible to specify `count` on a module and apply it to a collection, this module takes a collection of projects, which could be empty (allowing for conditional creation).

## Usage

See the examples directory for how to use the module.
To run the examples you need a `gitlab_token` of a user with access to the open-source-devex project, or you need to adapt the parent group ids in the examples before running them.
An easy way to provide the token is to create a _overrides_ file in the example directory:
```hcl
# secrets_override.tf
variable gitlab_token {
  default = "...."
}
```

## Known Issues

### Missing branch when creating gitlab_project
When the provisioning of a repository fails, subsequent terraform runs will break because the repo is not initialized and there will be no default branch created.
The errors look like this
```
Error: PUT https://gitlab.com/api/v4/projects/13656014: 400 {message: {base: [Could not change HEAD: branch 'master' does not exist]}}

  on ../../projects.tf line 1, in resource "gitlab_project" "default":
   1: resource "gitlab_project" "default" {
``` 

One way to workaround this is to create the default (`master`) branch manually in the repo. Another way is to delete the repo.
A structural fix would be to have an initial provisioning as part of the project resource that creates the default branch. 

### Duplicate fingerprint when creating gitlab_deploy_key
There is a timing issue when the same deploy key is added to two or more projects are the same time.
Gitlab will respond with an error:
```
Error: POST https://gitlab.com/api/v4/projects/13676761/deploy_keys: 400 {message: {deploy_key.fingerprint: [has already been taken]}}

  on ../../projects.tf line 51, in resource "gitlab_deploy_key" "default":
  51: resource "gitlab_deploy_key" "default" {
```
Subsequent terraform runs apply their plans cleanly.

One other way to workaround this issue is to create a deploy key in some other project. then reuse the key here and terraform will plan and apply in a single attempt.
