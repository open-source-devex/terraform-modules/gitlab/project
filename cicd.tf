resource "gitlab_project" "cicd" {
  count = local.has_cicd ? var.unique_groups : 0

  namespace_id = local.groups_ids[count.index]

  name = "cicd"
  path = var.cicd_project_path

  description = "CICD Automation for group repositories"

  visibility_level = var.default_visibility_level
  merge_method     = var.default_merge_method

  container_registry_enabled = false
  issues_enabled             = false
  wiki_enabled               = false
  snippets_enabled           = false

  default_branch = "master"
}

resource "null_resource" "setup_cicd" {
  count = local.has_cicd ? var.unique_groups : 0

  triggers = {
    project = gitlab_project.cicd[count.index].id
  }

  provisioner "local-exec" {
    command = <<EOC
set -e
PROJECT_DIR=$PWD;
cd ${local.tmp_dir}/${local.groups_ids[count.index]};
git clone ${gitlab_project.cicd[count.index].ssh_url_to_repo};
cd ${var.cicd_project_path};

$PROJECT_DIR/${path.module}/files/scripts/setup-repo.sh "cicd"
EOC
  }

  depends_on = [
    gitlab_project.cicd,
    null_resource.tmp_directory,
  ]
}
