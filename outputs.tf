output "repository_clone_urls_ssh" {
  value = [gitlab_project.default[*].ssh_url_to_repo]
}

locals {
  parent_id_to_clone_cmd = [for index in range(0, length(var.parent_group_ids)) : {"${var.parent_group_ids[index]}" = "[ ! -d ${gitlab_project.default[index].path} ] && git clone --recurse-submodules ${gitlab_project.default[index].ssh_url_to_repo};"} ]
}

output "repository_clone_urls_per_group" {
  value = {for id in distinct(var.parent_group_ids) : id => [for elem in local.parent_id_to_clone_cmd : values(elem)[0] if contains(keys(elem), id)]}
}
