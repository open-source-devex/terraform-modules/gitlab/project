variable parent_group_ids {
  type        = list(string)
  default     = []
  description = "The IDs of the groups under which projects are to be created. This list needs to have the same length as var.projects."
}

variable "cicd_project_path" {
  type    = string
  default = "0-cicd"
}

variable "create_cicd_deploy_key" {
  type    = bool
  default = false
}

variable "cicd_deploy_key" {
  type    = string
  default = ""
}

variable "cicd_deploy_key_name" {
  default = ""
}

variable projects {
  type = list(object({
    name                       = string,
    path                       = string,
    description                = string,
    project_kind               = string,
    visibility_level           = string,
    merge_method               = string,
    container_registry_enabled = bool,
    issues_enabled             = bool,
    wiki_enabled               = bool,
    snippets_enabled           = bool,
  }))

  default = []
}

variable default_visibility_level {
  type    = string
  default = "public"
}

variable default_merge_method {
  type    = string
  default = "rebase_merge"
}

variable "unique_groups" {
  type        = number
  default     = 0
  description = "This value is needed to make count attributes available at plan time."
}
