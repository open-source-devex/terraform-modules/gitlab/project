locals {
  tmp_dir = "/tmp/${uuid()}"

  has_cicd = var.cicd_project_path != ""

  groups_ids = distinct(var.parent_group_ids)

  project_count = length(var.projects)
}

resource "null_resource" "tmp_directory" {
  count = var.unique_groups

  triggers = {
    project = join(",", gitlab_project.default[*].id)
  }

  provisioner "local-exec" {
    command = "mkdir -p ${local.tmp_dir}/${local.groups_ids[count.index]}"
  }
}
