resource "gitlab_project" "default" {
  count = local.project_count

  namespace_id = var.parent_group_ids[count.index]

  name = var.projects[count.index]["name"]
  path = var.projects[count.index]["path"] != "" ? var.projects[count.index]["path"] : var.projects[count.index]["name"]

  description = var.projects[count.index]["description"] != "" ? var.projects[count.index]["description"] : "Project created with terraform"

  visibility_level = var.projects[count.index]["visibility_level"] != "" ? var.projects[count.index]["visibility_level"] : "public"
  merge_method     = var.projects[count.index]["merge_method"] != "" ? var.projects[count.index]["merge_method"] : "rebase_merge"

  container_registry_enabled = var.projects[count.index]["container_registry_enabled"]
  issues_enabled             = var.projects[count.index]["issues_enabled"]
  wiki_enabled               = var.projects[count.index]["wiki_enabled"]
  snippets_enabled           = var.projects[count.index]["snippets_enabled"]

  default_branch = "master"
}

resource "null_resource" "setup_projects" {
  count = local.project_count

  triggers = {
    project = gitlab_project.default[count.index].id
  }

  provisioner "local-exec" {
    command = <<EOC
set -e
PROJECT_DIR=$PWD;
cd ${local.tmp_dir}/${var.parent_group_ids[count.index]};
git clone ${gitlab_project.default[count.index].ssh_url_to_repo};
cd ${var.projects[count.index]["path"] != "" ? var.projects[count.index]["path"] : var.projects[count.index]["name"]};

$PROJECT_DIR/${path.module}/files/scripts/setup-repo.sh \
  "${var.projects[count.index]["project_kind"]}" \
  "${local.has_cicd ? "cicd" : ""}" \
   "${local.has_cicd ? "../${var.cicd_project_path}" : ""}"
EOC
  }

  depends_on = [
    gitlab_project.default,
    null_resource.tmp_directory,
    null_resource.setup_cicd,
  ]
}

resource "gitlab_deploy_key" "default" {
  count = var.create_cicd_deploy_key ? local.project_count : 0

  project = gitlab_project.default[count.index].id
  title   = var.cicd_deploy_key_name != "" ? var.cicd_deploy_key_name : "CICD Key - managed by terraform"
  key     = var.cicd_deploy_key

  can_push = true
}
